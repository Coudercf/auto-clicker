

class Scroll:

    def __init__(self, dy):
        self.direction = dy * 100

    def __repr__(self):
        return 'scroll %s' % 'top' if self.direction >= 1 else 'bottom'
