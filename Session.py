import pickle
from pynput import mouse
from pynput import keyboard
from threading import Thread, Event
from Scroll import Scroll
from Click import Click
from Input import Input
from Waiter import Waiter


class Session(Thread):

    def __init__(self, name):
        Thread.__init__(self)
        self.name = name
        self.keyboard_listener = keyboard.Listener(on_press=self.on_press, on_release=self.on_release)
        self.mouse_listener = mouse.Listener(on_click=self.on_click, on_scroll=self.on_scroll)
        self.wait_listener = Waiter(self.on_wait)
        self.session = []

    def run(self):
        self.keyboard_listener.start()
        self.mouse_listener.start()
        self.wait_listener.start()
        self.keyboard_listener.join()
        self.mouse_listener.join()
        self.wait_listener.join()

    def finish(self):
        self.keyboard_listener.stop()
        self.mouse_listener.stop()
        self.wait_listener.stop()
        self.save()

    def save(self):
        with open("save/%s.ses" % self.name, 'wb') as session_save:
            pickle.dump(self.session, session_save)
            print(self.session)

    def on_press(self, key):
        if key == keyboard.Key.esc:
            self.finish()
        elif key == keyboard.Key.shift:
            pass
        else:
            self.session.append(Input(key))

    def on_release(self, key):
        pass

    def on_click(self, x, y, button, pressed):
        if pressed:
            self.session.append(Click(x, y))

    def on_scroll(self, x, y, dx, dy):
        self.session.append(Scroll(dy))

    def on_wait(self, w):
        self.session.append(w)
