import os
import sys
import glob
import tkinter as tk
from Session import Session
from Auto import Auto

sessions = {}

for s in glob.glob('./save/*.ses'):
    name = os.path.basename(s)[:-4]
    sessions[name] = Session(name)


def start_session(name, sessions):
    session = Session(name)
    sessions[name] = session
    session.start()
    session.join


def stop_session(name, sessions):
    sessions[name].finish()


def start_auto(name):
    auto = Auto(name)
    auto.start()
    auto.join()


fenetre = tk.Tk()

value = tk.StringVar()
entree = tk.Entry(fenetre, width=30)
entree.pack()
bouton=tk.Button(fenetre, text="Enregistrer", command=lambda: start_session(entree.get(), sessions))
bouton.pack()
bouton=tk.Button(fenetre, text="Fin", command=lambda: stop_session(entree.get(), sessions))
bouton.pack()
bouton=tk.Button(fenetre, text="Start", command=lambda: start_auto(entree.get()))
bouton.pack()

fenetre.mainloop()
