import time
from threading import Thread, Event


class Waiter(Thread):

    def __init__(self, on_wait):
        Thread.__init__(self)
        self.on_wait = on_wait
        self._stop_event = Event()

    def run(self):
        while not self._stop_event.is_set():
            time.sleep(0.5)
            self.on_wait(0.5)

    def stop(self):
        self._stop_event.set()
